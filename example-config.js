// module.exports = [
//
//    // Plain port forwarding
//    {
//        listen: {
//            host: '0.0.0.0',
//            port: 80,
//        },
//        connect: {
//            host: '192.168.0.1',
//            port: 80,
//        },
//    },
//
//    // Listen to SSL, connect to plain
//    {
//        listen: {
//            host: '0.0.0.0',
//            port: 443,
//            ssl: {
//                key: 'ssl/key',
//                cert: 'ssl/cert',
//                ca: [
//                    'ssl/ca1',
//                    'ssl/ca2',
//                    ...
//                ],
//            },
//        },
//        connect: {
//            host: '192.168.0.1',
//            port: 80,
//        },
//    },
//
//    // Listen to plain, connect to SSL
//    {
//        listen: {
//            host: '0.0.0.0',
//            port: 80,
//        },
//        connect: {
//            host: '192.168.0.1',
//            port: 443,
//            ssl: true,
//        },
//    },
//
//    // Listen to plain, connect to SSL but ignore certificate errors
//    {
//        listen: {
//            host: '0.0.0.0',
//            port: 80,
//        },
//        connect: {
//            host: '192.168.0.1',
//            port: 443,
//            ssl: {
//                insecure: true,
//            },
//        },
//    },
//
//    ...
//
//]
