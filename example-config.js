// module.exports = {
//     debug_mode: true,
//     links: [
//
//         // Plain port forwarding
//         {
//             port: 80,
//             connect: '192.168.0.1',
//         },
//
//         // Plain port forwarding, listening on a different IP address
//         {
//             port: 80,
//             listen: '0.0.0.0',
//             connect: '192.168.0.1',
//         },
//
//         // Plain port forwarding, listening on a different port
//         {
//             listen: '0.0.0.0:8080',
//             connect: '192.168.0.1:8081',
//         },
//
//         // Listen to SSL, connect to plain
//         {
//             listen: {
//                 host: '0.0.0.0',
//                 port: 443,
//                 ssl: {
//                     key: 'ssl/key',
//                     cert: 'ssl/cert',
//                     ca: [
//                         'ssl/ca1',
//                         'ssl/ca2',
//                         ...
//                     ],
//                 },
//             },
//             connect: '192.168.0.1:80',
//         },
//
//         // Listen to plain, connect to SSL
//         {
//             listen: '0.0.0.0:80',
//             connect: {
//                 host: '192.168.0.1',
//                 port: 443,
//                 ssl: true,
//                 // indicate host name
//                 name: 'example.com',
//             },
//         },
//
//         // Listen to plain, connect to SSL but ignore certificate errors
//         {
//             listen: '0.0.0.0:80',
//             connect: {
//                 host: '192.168.0.1',
//                 port: 443,
//                 ssl: {
//                     insecure: true,
//                 },
//             },
//         },
//
//         ...
//
//     ],
// }
