const PlainClient = require('./lib/PlainClient')
const PlainServer = require('./lib/PlainServer')
const SecureClient = require('./lib/SecureClient')
const SecureServer = require('./lib/SecureServer')

require('./config').forEach(pair => {

    const listen = pair.listen
    const connect = pair.connect

    const Server = listen.ssl === undefined ? PlainServer : SecureServer
    const Client = connect.ssl === undefined ? PlainClient : SecureClient

    Server(listen, server_socket => {
        Client(connect, server_socket, client_socket => {
            server_socket.pipe(client_socket)
            client_socket.pipe(server_socket)
        }, err => {
            console.log((new Date).toISOString(), 'ERROR Connection failed: ' + err.code)
            server_socket.end()
        })
    })

})
