const tls = require('tls')

module.exports = (connect, server_socket, pipe, error) => {
    const client_socket = tls.connect({
        host: connect.host,
        port: connect.port,
        rejectUnauthorized: connect.ssl.insecure !== true,
    }, () => {
        pipe(client_socket)
    })
    client_socket.on('error', error)
}
