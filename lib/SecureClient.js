const tls = require('tls')

module.exports = (connect, pipe) => tls.connect({
    host: connect.host,
    port: connect.port,
    rejectUnauthorized: connect.ssl.insecure !== true,
    servername: connect.name,
}, pipe)
