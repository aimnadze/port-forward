module.exports = app => () => {

    app.config.links.forEach((pair, index) => {

        const port = pair.port

        const listen = app.Normalize(pair.listen, pair.port)
        if (listen.port === undefined) listen.port = port

        const connect = app.Normalize(pair.connect, pair.port)
        if (connect.port === undefined) connect.port = port

        const Server = listen.ssl === undefined ? app.PlainServer : app.SecureServer
        const Client = connect.ssl === undefined ? app.PlainClient : app.SecureClient

        const log = app.log.sublog('Link #' + (index + 1))
        log.info('Forwarding ' + listen.host +
            (listen.port === undefined ? '' : ':' + listen.port) +
            ' to ' + connect.host +
            (connect.port === undefined ? '' : ':' + connect.port))

        Server(listen, server_socket => {
            const client_socket = Client(connect, () => {
                server_socket.pipe(client_socket)
                client_socket.pipe(server_socket)
            })
            client_socket.on('error', err => {
                log.error('Failed: ' + err.code)
                server_socket.end()
            })
            server_socket.on('end', () => {
                client_socket.end()
            })
            server_socket.on('error', () => {
                client_socket.end()
            })
        })

    })

    app.Watch(['lib'])

    app.log.info('Started')

}
