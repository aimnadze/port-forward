module.exports = () => {

    function Log (preargs) {
        return {
            error (...args) {
                console.log((new Date).toISOString(), 'ERROR', ...preargs, ...args)
            },
            info (...args) {
                console.log((new Date).toISOString(), 'INFO', ...preargs, ...args)
            },
            sublog (prefix) {
                return Log([...preargs, prefix])
            },
        }
    }

    return Log([])

}
