const net = require('net')

module.exports = (listen, pipe) => {
    net.createServer(pipe).listen(listen.port, listen.host)
}
