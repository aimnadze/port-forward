const fs = require('fs')
const tls = require('tls')

module.exports = (listen, pipe) => {

    const ssl = listen.ssl

    tls.createServer({
        key: fs.readFileSync(ssl.key),
        cert: fs.readFileSync(ssl.cert),
        cs: (() => {
            const ca = Object(ssl.ca)
            return Object.keys(ca).map(i => {
                return fs.readFileSync(ca[i], 'utf8')
            })
        })(),
    }, pipe).listen(listen.port, listen.host)

}
