module.exports = (value, default_port) => {

    if (value === undefined) return { host: 'localhost' }

    if (typeof value === 'number') return { host: 'localhost', port: value }

    if (typeof value === 'object') return value

    const match = value.match(/^(.*):(\d+)$/)
    if (match === null) return { host: value, port: default_port }

    return {
        host: match[1],
        port: Number(match[2]),
    }

}
