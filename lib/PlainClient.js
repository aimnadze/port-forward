const net = require('net')

module.exports = (connect, server_socket, pipe, error) => {
    const client_socket = net.connect({
        host: connect.host,
        port: connect.port,
    }, () => {
        pipe(client_socket)
    })
    client_socket.on('error', error)
}
