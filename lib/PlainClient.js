const net = require('net')

module.exports = (connect, pipe) => net.connect({
    host: connect.host,
    port: connect.port,
}, pipe)
