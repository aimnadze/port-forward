Port Forward
============

TCP port forwarding is such a simple task, yet there
are no simple tools for setting it up. With Port Forward you can
setup a fully functional port forwarding server in less than a minute.

Features
--------

* Accepting connections over either plain or secure SSL socket
* Forwarding to either plain or secure SSL socket

Use cases
---------

Plenty of use cases of port forwarding exist
and are documented better elsewhere.
Listing them here would a copy-paste.
However, there are some use cases that Port Forward is good at.
The source code is too small (less than 200 lines) and with
a little bit of modification it can easily be converted into:

* a traffic monitor,
* an access control manager, or even
* a load balancer.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.

Configuration
-------------

`config.js` contains the configuration. See `example-config.js` for details.

See Also
--------

* [Reverse Proxy Server](https://gitlab.com/aimnadze/reverse-proxy-server)
* [Forward Proxy Server](https://gitlab.com/aimnadze/forward-proxy-server)
